import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, SDO, RDFS, OWL, PROV
import urllib.parse

# actor
df=pd.read_excel('../data/source/actor.xls')
df.to_csv('../data/derived/actor.csv', index=False)

# cargo
df=pd.read_excel('../data/source/cargo.xls')
df.to_csv('../data/derived/cargo.csv', index=False)

# slaves group
df=pd.read_excel('../data/source/slaves_group.xls')
df.to_csv('../data/derived/slaves_group.csv', index=False)

# main ESTA
df=pd.read_excel('../data/source/mainESTA.xlsx')
df.to_csv('../data/derived/mainESTA.csv', index=False)

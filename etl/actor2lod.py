import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, SDO, RDFS, OWL, PROV
import urllib.parse

df=pd.read_csv('../data/derived/actor.csv')
#print(df)

# some data updating -> voyage and subvoyage have similar ids
df['voyage_id'] = 't'+df['voyage_id'].astype(str)
df['subvoyage_id'] = 's'+df['subvoyage_id'].astype(str)
df['actor_id'] = 'a'+df['actor_id'].astype(str)
df['actor_sex'] = df['actor_sex'].astype(str).str.capitalize()
df['actor_source'] = df['actor_source'].astype(str)
#df['cargo_details'] = df['cargo_commodity'].astype(str)+ '-'+ df['cargo_quantity'].astype(str) + '_' + df['cargo_source'].astype(str)
#print(df)

# for modeling check out sealit ontology: https://zenodo.org/record/5964240
# unfortunately this data or not modeled by point of entry and departure, but by 'subvoyage'
# this means that terms in the ontology such as loading and unloading are not possible to model

# I consider a subvoyage to be a 'voyage' as defined by sealit
# to deal with 'voyage', I'll call that a transvoyage

g = Graph()
esta = Namespace('https://iisg.amsterdam/id/esta/') # for the dataset
vesta = Namespace('http://iisg.amsterdam/vocab/esta/') # for terms in the vocab
sea = Namespace('https://sealitproject.eu/ontology/') # http://users.ics.forth.gr/~fafalios/files/pubs/fafaliosJOCCH2021.pdf
schema = Namespace('https://schema.org/')

# first lets add classes (that are not specified in the excel file)
# some vocabs such as OWL are in the RDFLIB (and we can define easiliy)
# for others we ned the function URIRef to create URI's

g.add((URIRef(vesta+'Transvoyage'), RDF.type, OWL.Class))
g.add((URIRef(vesta+'Transvoyage'), RDFS.subClassOf, OWL.Thing))
g.add((URIRef(sea+'Voyage'), RDFS.subClassOf, URIRef(vesta+'Transvoyage')))



for index, row in df.iterrows():
# voyage and subvoyage
    g.add((URIRef(esta+(row['voyage_id'])), RDF.type, URIRef(vesta+'Transvoyage')))
    g.add((URIRef(esta+(row['subvoyage_id'])), RDF.type, URIRef(sea+'Voyage')))
    g.add((URIRef(esta+(row['subvoyage_id'])), SDO.isPartOf, URIRef(esta+str(row['voyage_id']))))

# actor
    g.add((URIRef(esta+(row['actor_id'])), RDF.type, SDO.Person))
    g.add((URIRef(esta+(row['actor_id'])), SDO.gender, URIRef(schema+(row['actor_sex']))))
    g.add((URIRef(esta+(row['subvoyage_id'])), URIRef(sea+'hadParticipant'), URIRef(esta+row['actor_id'])))
    g.add((URIRef(esta+(row['actor_id'])), PROV.wasDerivedFrom, Literal(row['actor_source'], datatype=XSD.string)))
    g.add((URIRef(esta+(row['actor_id'])), SDO.name, Literal(row['actor_name'], datatype=XSD.string)))
    g.add((URIRef(esta+(row['actor_id'])), SDO.role, Literal(row['actor_role'], datatype=XSD.string)))
    g.add((URIRef(esta+(row['actor_id'])), RDFS.comment, Literal(row['actor_notes'], datatype=XSD.string)))
    g.add((URIRef(esta+(row['actor_id'])), RDFS.comment, Literal(row['actor_description'], datatype=XSD.string)))
      

'''
    g.add((URIRef(hkh+row['registrationId']), SDO.familyName, Literal(row['familienaam'], datatype=XSD.string))) #still contains surnamePrefix
    g.add((URIRef(hkh+row['registrationId']), SDO.birthDate, Literal(row['geboorteDatumCl'], datatype=XSD.date)))
    g.add((URIRef(hkh+row['registrationId']), SDO.occupation, Literal(row['beroep'], lang='nl')))
    g.add((URIRef(hkh+row['registrationId']), RDFS.comment, Literal(row['opmerkingen'], lang='nl')))
    g.add((URIRef(hkh+row['registrationId']), URIRef(pnv+'givenName'), Literal(row['voornaam'], datatype=XSD.string)))
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'hasReligion'), Literal(row['religie'], datatype=XSD.string)))
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'verblijf'), Literal(row['verblijf'], datatype=XSD.string))) 
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'datumInschrijvingCl'), Literal(row['datumInschrijvingCl'], datatype=XSD.date)))
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'vertrekDatumCl'), Literal(row['vertrekDatumCl'], datatype=XSD.date))) 
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'burgerlijkeStaat'), Literal(row['burgerlijkeStaat'], datatype=XSD.string))) 
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'vorigeWoonplaats'), Literal(row['vorigeWoonplaats'], datatype=XSD.string))) 
    g.add((URIRef(hkh+row['registrationId']), URIRef(cbgo+'blad'), Literal(row['blad'], datatype=XSD.string))) 
'''
print(g.serialize(format='nt'))
g.serialize('../data/derived/actor.nt',format='nt')
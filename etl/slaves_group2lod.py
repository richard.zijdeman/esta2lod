import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import FOAF, XSD, SDO, RDFS, OWL, PROV
import urllib.parse

df=pd.read_csv('../data/derived/slaves_group.csv')
print(df)

# some data updating -> voyage and subvoyage have similar ids
df['voyage_id'] = 't'+df['voyage_id'].astype(str)
df['subvoyage_id'] = 's'+df['subvoyage_id'].astype(str)
df['group_id'] = 'gr'+df['group_id'].astype(str)
df['slaves_id'] = 'slv'+df['slaves_id'].astype(str)
df['gr_sex'] = df['gr_sex'].astype(str).str.capitalize()

# for modeling check out sealit ontology: https://zenodo.org/record/5964240
# unfortunately this data or not modeled by point of entry and departure, but by 'subvoyage'
# this means that terms in the ontology such as loading and unloading are not possible to model

# I consider a subvoyage to be a 'voyage' as defined by sealit
# to deal with 'voyage', I'll call that a transvoyage

g = Graph()
esta = Namespace('https://iisg.amsterdam/id/esta/') # for the dataset
vesta = Namespace('http://iisg.amsterdam/vocab/esta/') # for terms in the vocab
sea = Namespace('https://sealitproject.eu/ontology/') # http://users.ics.forth.gr/~fafalios/files/pubs/fafaliosJOCCH2021.pdf
schema = Namespace('https://schema.org/')

# first lets add classes (that are not specified in the excel file)
# some vocabs such as OWL are in the RDFLIB (and we can define easiliy)
# for others we ned the function URIRef to create URI's

g.add((URIRef(vesta+'Transvoyage'), RDF.type, OWL.Class))
g.add((URIRef(vesta+'Transvoyage'), RDFS.subClassOf, OWL.Thing))
g.add((URIRef(sea+'Voyage'), RDFS.subClassOf, URIRef(vesta+'Transvoyage')))



for index, row in df.iterrows():
# voyage and subvoyage
    g.add((URIRef(esta+(row['voyage_id'])), RDF.type, URIRef(vesta+'Transvoyage')))
    g.add((URIRef(esta+(row['subvoyage_id'])), RDF.type, URIRef(sea+'Voyage')))
    g.add((URIRef(esta+(row['subvoyage_id'])), SDO.isPartOf, URIRef(esta+str(row['voyage_id']))))

# slaves_group
    g.add((URIRef(esta+(row['subvoyage_id'])), URIRef(vesta+'containsGroup'), URIRef(esta+row['group_id'])))
    g.add((URIRef(esta+(row['group_id'])), OWL.sameAs, URIRef(esta+row['slaves_id'])))
    # conditional formatting for values in gr_sex, see: https://schema.org/gender and https://schema.org/Female
    for sex in row['gr_sex']:
        if (sex=='Male') | (sex=='Female'):
            g.add((URIRef(esta+(row['group_id'])), SDO.gender, URIRef(schema+row(['gr_sex'])))) # e.g. schema:Male
        else:
            g.add((URIRef(esta+(row['group_id'])), SDO.gender, Literal(str(row['gr_sex']), datatype=XSD.string))) # e.g. "Mixed^^<xsd:string>"
    
    g.add((URIRef(esta+(row['group_id'])), URIRef(vesta+'hasAgeGroup'), Literal(str(row['gr_age_group']), datatype=XSD.string)))
    g.add((URIRef(esta+(row['group_id'])), URIRef('https://dbpedia.org/ontology/ethnicity'), Literal(str(row['gr_ethnicity']), datatype=XSD.string)))
    g.add((URIRef(esta+(row['group_id'])), URIRef(vesta+'hasPhysicalState'), Literal(str(row['gr_physical_state']), datatype=XSD.string)))
    # NB: because of string in gr_quantity, I can't make it integer
    g.add((URIRef(esta+(row['group_id'])), URIRef(vesta+'hasGroupSize'), Literal(str(row['gr_quantity']), datatype=XSD.string)))
    g.add((URIRef(esta+(row['group_id'])), RDFS.comment, Literal(str(row['gr_notes']), datatype=XSD.string)))
     
print(g.serialize(format='nt'))
g.serialize('../data/derived/slaves_group.nt',format='nt')